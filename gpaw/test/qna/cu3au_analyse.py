import numpy as np

data = np.loadtxt('data.txt')
print data
x = data[:,0]
E = data[:,1]
F = data[:,2]

# Calculate central derivatives
dx = x[1]-x[0]
numF = -(E[2:] - E[:-2]) / (2*dx)
print "Numerical forces", numF
print "Analytic forces", F[1:-1]
x = x[1:-1]
import matplotlib.pyplot as plt
plt.plot(x, numF, 'o',label="Numerical")
plt.plot(x, F[1:-1], 'x', label="Analytical")
plt.legend()
plt.show()
