import numpy as np
from gpaw import GPAW, PW
from ase import Atoms
from ase.eos import EquationOfState as EOS
from ase.units import *
from ase.io import write
from gpaw import GPAW, FermiDirac
from gpaw.mpi import world
from gpaw.xc.gga import PurePythonGGAKernel, GGA
from gpaw.xc.qna import QNA
from sys import argv
from ase.optimize import BFGS

alpha = float(argv[1])

system = 'AuCu3'
ecut = 700 # eV
kpts = (12,12,12)
gpts = (24,24,24)
smear_width = 0.01 #eV
n_atoms = 4

#xc = 'LDA'
#xc = 'PBE'
xc = 'QNA'
qna_parameters  = {'Au': (0.1250,0.1000), 'Cu':(0.0795, 0.0050)}
#qna_parameters  = {'Au': (0.1250,0.1000), 'Cu':(0.125, 0.0050)}
qna_setup_name = 'PBE'

# Estimate for the equilibrium lattice constant (in angstroms):
if xc == 'LDA' or xc == 'QNA':
    a = 3.74
elif xc == 'PBE' or xc == 'pyPBE':
    a = 3.8

bulk = Atoms(system,
             positions=[[0,0,0],[0.5,0.5,0],[0.5,0,0.5],[0,0.5,0.5]],
             pbc=True)
bulk.set_cell(cell=[[a, 0, 0],[0, a, 0],[0, 0, a]],scale_atoms=True)

cell0 = bulk.get_cell()
name = '%s-fcc-%s' % (system, xc.lower())

#ens = []
#vols = []

calc = GPAW(mode=PW(ecut),
            xc = QNA(bulk, qna_parameters, qna_setup_name, alpha=alpha),
            gpts=gpts,
            kpts=kpts,
            occupations=FermiDirac(smear_width),
            txt=name + '.txt')

# Set some unrelaxed positions:
bulk[0].position[2] += 0.2
bulk[1].position[0] += -0.2
bulk[2].position[1] += -0.3
bulk[3].position[0] += 0.2
bulk.set_calculator(calc)

dyn = BFGS(bulk, trajectory='{0}-{1}.traj'.format(system,xc))
dyn.run(fmax=0.05)



"""
f = open('dataQNA%.2f.txt' % alpha,'w')
for n, x in enumerate(np.linspace(-0.3, 0.3, 25)):
    bulk[0].position[0] = x
    E = bulk.get_potential_energy()
    F = bulk.get_forces()
    print >>f, x, E, F[0][0]
    f.flush()
"""
