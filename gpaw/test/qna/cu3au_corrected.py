import numpy as np
from gpaw import GPAW, PW
from ase import Atoms
from ase.eos import EquationOfState as EOS
from ase.units import *
from ase.io import write
from gpaw import GPAW, FermiDirac
from gpaw.mpi import world
from gpaw.xc.gga import PurePythonGGAKernel, GGA
from gpaw.xc.qna import QNA
from sys import argv

alpha = float(argv[1])

system = 'AuCu3'
ecut = 400 # eV
kpts = (2,2,2)
gpts = (16,16,16)
smear_width = 0.01 #eV
n_atoms = 4

#xc = 'LDA'
#xc = 'PBE'
xc = 'QNA'
qna_parameters  = {'Au': (0.1250,0.1000), 'Cu':(0.0795, 0.0050)}
qna_setup_name = 'PBE'

# Estimate for the equilibrium lattice constant (in angstroms):
if xc == 'LDA' or xc == 'QNA':
    a = 3.74
elif xc == 'PBE':
    a = 3.8

bulk = Atoms(system,
             positions=[[0,0,0],[0.5,0.5,0],[0.5,0,0.5],[0,0.5,0.5]],
             pbc=True)
bulk.set_cell(cell=[[a, 0, 0],[0, a, 0],[0, 0, a]],scale_atoms=True)

cell0 = bulk.get_cell()
name = '%s-fcc-%s' % (system, xc.lower())

ens = []
vols = []

# Volume range taken from the famous science paper (Lejaeghere et al.):

if 1:
    calc = GPAW(mode=PW(ecut),
                xc = QNA(bulk, qna_parameters, qna_setup_name, alpha=alpha),
                gpts=gpts,
                kpts=kpts,
                occupations=FermiDirac(smear_width),
                txt=name + '.txt')
    bulk[0].position[0] = -0.3
    bulk.set_calculator(calc)
    f = open('corrQNA%.2f.txt' % alpha,'w')
    for n, x in enumerate(np.linspace(-0.3, 0.3, 25)):
        bulk[0].position[0] = x
        E = bulk.get_potential_energy()
        F = bulk.get_forces()
        displaced = bulk.copy()
        h = 0.01
        displaced[0].position[0] += h
        xc = QNA(bulk, qna_parameters, qna_setup_name, alpha=alpha, force_atoms=displaced)
        correction = calc.get_xc_difference(xc) / h
        print >>f, x, E, F[0][0], correction, F[0][0] + correction
        f.flush()
